---
title: News website design prototype 
mainfont: IBMPlexSans-Light
---

![](news-aggreg.jpg)

## Introduction

In this final UI-Prototyping project, you will design the front-end of a news aggregator website.

> *aggregation: a group, body, or mass composed of many distinct parts or individuals* (Merriam-webster)

You will be expected to show your abilities in user journey mapping, wire-framing, ui-design, consistency in design and prototyping to accomplish this project.

Research and implement your designs using known UI Patterns, from your own observations / examples or from sample sites like https://ui-patterns.com/patterns or http://www.welie.com/patterns/index.php. Twitter Bootstrap can also provide starting templates to develop adapted designs.

Your high fidelity prototypes will be rendered at two different screen sizes, on mobile (360x640px) and on small-desktop (approx 12.5inch display: 1366x768px).

Considering the topic of the project, aim to make a neutral, clear, communicative interface that focuses on legibility and clarity. 

## Deliverables

1. website architecture graph (a type of user-journey map that shows how the pages of the website relate to one another), delivery in pdf
2. low fidelity wireframes (hand drawn), delivery in pdf
3. medium fidelity prototype (using the tool of your choice), delivery in pdf
4. high fidelity prototype (using penpot or HTML+CSS) delivery in penpot document or link to Gitlab repository.

## Methods

Proceed logically from the first to the last deliverable, but allow for some back-and-forth in the design process, your early wireframes will inform your website architecture which in turn influences your designs, etc.

- diagrams.net / draw.io
- graphviz
- hand-drawn wireframes
- digital wireframes ('pencil' app)
- inkscape
- penpot
- HTML / CSS / JS


## Grading matrix

- clarity, realism and use of the architecture graph
- design research, iterations, use of existing design patterns
- consistency of the designs (towards deliverable 3/4, demonstrate a refining of your design research towards consistent design)
- neutral, clear, communicative interface with focus on legibility and clarity
- quality of final design proposal
- timely submissions, engagement during lab-hours, ability to take feedback onboard, technical document requirements met (document types, screen sizes, etc)

## Schedule

* Full project briefing: Tuesday 15th March
* note Spring break dates: 11 April to 24th April
* Intermediary delivery: 05 April — deliverables 1, 2 and 3 in draft form, submission via OneDrive folder 
* Final delivery: Friday 29 April — all deliverables in final digital form, submission via OneDrive folder


---


This repo contains a RSS reader written in JavaScript.

Thanks to https://github.com/hongkiat/js-rss-reader/ for the original code base.
  
>💁 If you're getting `Cross origin requests are only supported for HTTP` error while opening the HTML with `file:///` protocol, just open and save the HTML file using a text editor, that should resolve the issue. Or, for a quick test, you can also use a browser add-on such as [CORS Everywhere for Firefox](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/).
